#!/bin/bash

awk '
BEGIN {	FS=", ";}
{
	print "define(NAME_OF_USER," $1 ")dnl" > "def";
	print "define(EMAIL_OF_USER," $2 ")dnl" >> "def";
	print "define(PAN_OF_USER," $3 ")dnl" >> "def";
	print "define(DONATION_OF_USER," $4 ")dnl" >> "def";
	system("m4 def user_mail.m4 | msmtp -t " $2);
	close("def");
}' csvfile.csv

