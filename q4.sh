#!/bin/bash


starting_credentials='echo smtp.cse.iitk.ac.in
MAIL FROM:<kartk@cse.iitk.ac.in>
RCPT TO:<EMAIL_OF_USER>
DATA
From: <kartk@cse.iitk.ac.in>
Content-Type: multipart/mixed; boundary="BOUNDARYTEXT"
Subject:80G LETTER
To: <EMAIL_OF_USER>

--BOUNDARYTEXT
Content-Transfer-Encoding: quoted-printable
Content-Type: text/plain;
'

how_it_looks='


--BOUNDARYTEXT
Content-Disposition: inline; filename=80g.pdf
Content-Type: application/pdf; x-unix-mode=0644; name="80g.pdf"
Content-Transfer-Encoding: base64
'

echo "$starting_credentials" > final.m4;
cat 80g.mail.m4>> final.m4;
echo "$how_it_looks" >> final.m4;


awk '
BEGIN { FS=", "; }
{ print "define(NAME_OF_USER,"$1")dnl" > "def";
  print "define(EMAIL_OF_USER,"$2")dnl" >> "def";
  print "define(PAN_OF_USER,"$3")dnl" >> "def";
  print "define(DONATION_OF_USER,"$4")dnl" >> "def";
  system("m4 def 80g.tex.m4 > final_attachment.tex");
  system("pdflatex final_attachment.tex");
  system("m4 def final.m4 > finalbody");
  system("cat finalbody >finalmail");
  system("(cat final_attachment.pdf|base64) >> finalmail");
  print "--text_ends--" >>"finalmail";
  print "\n.\n" >> "finalmail";
  print "quit" >> "finalmail";
  system("cat finalmail | nc smtp.cse.iitk.ac.in 25")
  close("finalbody");
  close("def");
  close("final_attachment.tex");
  close("finalmail");
}
END{}' csvfile.csv